﻿# TODO: Translation updated at 2018-03-28 12:56

translate italian strings:

    # 00action_file.rpy:26
    old "{#weekday}Monday"
    new "{#weekday}Lunedì"

    # 00action_file.rpy:26
    old "{#weekday}Tuesday"
    new "{#weekday}Martedì"

    # 00action_file.rpy:26
    old "{#weekday}Wednesday"
    new "{#weekday}Mercoledì"

    # 00action_file.rpy:26
    old "{#weekday}Thursday"
    new "{#weekday}Giovedì"

    # 00action_file.rpy:26
    old "{#weekday}Friday"
    new "{#weekday}Venerdì"

    # 00action_file.rpy:26
    old "{#weekday}Saturday"
    new "{#weekday}Sabato"

    # 00action_file.rpy:26
    old "{#weekday}Sunday"
    new "{#weekday}Domenica"

    # 00action_file.rpy:37
    old "{#weekday_short}Mon"
    new "{#weekday_short}Lun"

    # 00action_file.rpy:37
    old "{#weekday_short}Tue"
    new "{#weekday_short}Mar"

    # 00action_file.rpy:37
    old "{#weekday_short}Wed"
    new "{#weekday_short}Mer"

    # 00action_file.rpy:37
    old "{#weekday_short}Thu"
    new "{#weekday_short}Gio"

    # 00action_file.rpy:37
    old "{#weekday_short}Fri"
    new "{#weekday_short}Ven"

    # 00action_file.rpy:37
    old "{#weekday_short}Sat"
    new "{#weekday_short}Sab"

    # 00action_file.rpy:37
    old "{#weekday_short}Sun"
    new "{#weekday_short}Dom"

    # 00action_file.rpy:47
    old "{#month}January"
    new "{#month}Gennaio"

    # 00action_file.rpy:47
    old "{#month}February"
    new "{#month}Febbraio"

    # 00action_file.rpy:47
    old "{#month}March"
    new "{#month}Marzo"

    # 00action_file.rpy:47
    old "{#month}April"
    new "{#month}Aprile"

    # 00action_file.rpy:47
    old "{#month}May"
    new "{#month}Maggio"

    # 00action_file.rpy:47
    old "{#month}June"
    new "{#month}Giugno"

    # 00action_file.rpy:47
    old "{#month}July"
    new "{#month}Luglio"

    # 00action_file.rpy:47
    old "{#month}August"
    new "{#month}Agosto"

    # 00action_file.rpy:47
    old "{#month}September"
    new "{#month}Settembre"

    # 00action_file.rpy:47
    old "{#month}October"
    new "{#month}Ottobre"

    # 00action_file.rpy:47
    old "{#month}November"
    new "{#month}Novembre"

    # 00action_file.rpy:47
    old "{#month}December"
    new "{#month}Dicembre"

    # 00action_file.rpy:63
    old "{#month_short}Jan"
    new "{#month_short}Gen"

    # 00action_file.rpy:63
    old "{#month_short}Feb"
    new "{#month_short}Feb"

    # 00action_file.rpy:63
    old "{#month_short}Mar"
    new "{#month_short}Mar"

    # 00action_file.rpy:63
    old "{#month_short}Apr"
    new "{#month_short}Apr"

    # 00action_file.rpy:63
    old "{#month_short}May"
    new "{#month_short}Mag"

    # 00action_file.rpy:63
    old "{#month_short}Jun"
    new "{#month_short}Giu"

    # 00action_file.rpy:63
    old "{#month_short}Jul"
    new "{#month_short}Lug"

    # 00action_file.rpy:63
    old "{#month_short}Aug"
    new "{#month_short}Ago"

    # 00action_file.rpy:63
    old "{#month_short}Sep"
    new "{#month_short}Set"

    # 00action_file.rpy:63
    old "{#month_short}Oct"
    new "{#month_short}Ott"

    # 00action_file.rpy:63
    old "{#month_short}Nov"
    new "{#month_short}Nov"

    # 00action_file.rpy:63
    old "{#month_short}Dec"
    new "{#month_short}Dic"

    # 00action_file.rpy:237
    old "%b %d, %H:%M"
    new "%b %d, %H:%M"

    # 00action_file.rpy:852
    old "Quick save complete."
    new "Salvataggio rapido completato."

    # 00director.rpy:703
    old "The interactive director is not enabled here."
    new "The interactive director is not enabled here."

    # 00director.rpy:1490
    old "Done"
    new "Done"

    # 00director.rpy:1498
    old "(statement)"
    new "(statement)"

    # 00director.rpy:1499
    old "(tag)"
    new "(tag)"

    # 00director.rpy:1500
    old "(attributes)"
    new "(attributes)"

    # 00director.rpy:1501
    old "(transform)"
    new "(transform)"

    # 00director.rpy:1526
    old "(transition)"
    new "(transition)"

    # 00director.rpy:1538
    old "(channel)"
    new "(channel)"

    # 00director.rpy:1539
    old "(filename)"
    new "(filename)"

    # 00director.rpy:1564
    old "Change"
    new "Change"

    # 00director.rpy:1566
    old "Add"
    new "Add"

    # 00director.rpy:1569
    old "Cancel"
    new "Annulla"

    # 00director.rpy:1572
    old "Remove"
    new "Rimuovere"

    # 00director.rpy:1605
    old "Statement:"
    new "Statement:"

    # 00director.rpy:1626
    old "Tag:"
    new "Tag:"

    # 00director.rpy:1642
    old "Attributes:"
    new "Attributes:"

    # 00director.rpy:1660
    old "Transforms:"
    new "Transforms:"

    # 00director.rpy:1679
    old "Behind:"
    new "Behind:"

    # 00director.rpy:1698
    old "Transition:"
    new "Transition:"

    # 00director.rpy:1716
    old "Channel:"
    new "Channel:"

    # 00director.rpy:1734
    old "Audio Filename:"
    new "Audio Filename:"

    # 00gui.rpy:368
    old "Are you sure?"
    new "Sei sicuro?"

    # 00gui.rpy:369
    old "Are you sure you want to delete this save?"
    new "Sei sicuro di voler eliminare questo salvataggio?"

    # 00gui.rpy:370
    old "Are you sure you want to overwrite your save?"
    new "Sei sicuro di voler sovrascrivere il tuo salvataggio?"

    # 00gui.rpy:371
    old "Loading will lose unsaved progress.\nAre you sure you want to do this?"
    new "Il caricamento porterà alla perdita dei progressi non salvati.\nSei sicuro di voler fare questo?"

    # 00gui.rpy:372
    old "Are you sure you want to quit?"
    new "Sei sicuro di voler uscire?"

    # 00gui.rpy:373
    old "Are you sure you want to return to the main menu?\nThis will lose unsaved progress."
    new "Sei sicuro di voler tornare al menu principale?\nQuesto porterà alla perdita dei progressi non salvati."

    # 00gui.rpy:374
    old "Are you sure you want to end the replay?"
    new "Sei sicuro di voler terminare il replay?"

    # 00gui.rpy:375
    old "Are you sure you want to begin skipping?"
    new "Sei sicuro di voler iniziare a saltare?"

    # 00gui.rpy:376
    old "Are you sure you want to skip to the next choice?"
    new "Sei sicuro di voler saltare fino alla prossima scelta?"

    # 00gui.rpy:377
    old "Are you sure you want to skip unseen dialogue to the next choice?"
    new "Sei sicuro di voler saltare il dialogo non visto fino alla prossima scelta?"

    # 00keymap.rpy:258
    old "Failed to save screenshot as %s."
    new "Impossibile salvare screenshot come %s."

    # 00keymap.rpy:270
    old "Saved screenshot as %s."
    new "Screenshot salvato come %s."

    # 00library.rpy:146
    old "Self-voicing disabled."
    new "Sintesi vocale disabilitata."

    # 00library.rpy:147
    old "Clipboard voicing enabled. "
    new "Clipboard voicing enabled."

    # 00library.rpy:148
    old "Self-voicing enabled. "
    new "Sintesi vocale abilitata."

    # 00library.rpy:183
    old "Skip Mode"
    new "Modalità di salto"

    # 00library.rpy:269
    old "This program contains free software under a number of licenses, including the MIT License and GNU Lesser General Public License. A complete list of software, including links to full source code, can be found {a=https://www.renpy.org/l/license}here{/a}."
    new "Questo programma contiene software gratuito con un certo numero di licenze, tra cui la Licenza MIT e la Licenza Pubblica Generale Minore GNU. Un elenco completo di software, compresi i collegamenti al codice sorgente completo, può essere trovato {a=https://www.renpy.org/l/license}qui{/a}."

    # 00preferences.rpy:475
    old "Clipboard voicing enabled. Press 'shift+C' to disable."
    new "Voce di Appunti abilitata. Premi 'shift + C' per disabilitare."

    # 00preferences.rpy:477
    old "Self-voicing would say \"[renpy.display.tts.last]\". Press 'alt+shift+V' to disable."
    new "Sintesi vocale direbbe \"[renpy.display.tts.last]\". Premi 'alt + shift + V' per disabilitare."

    # 00preferences.rpy:479
    old "Self-voicing enabled. Press 'v' to disable."
    new "Sintesi vocale abilitata. Premere 'V' per disabilitare."

    # _compat\gamemenu.rpym:198
    old "Empty Slot."
    new "Slot vuoto."

    # _compat\gamemenu.rpym:355
    old "Previous"
    new "Precedente"

    # _compat\gamemenu.rpym:362
    old "Next"
    new "Prossimo"

    # _compat\preferences.rpym:428
    old "Joystick Mapping"
    new "Mappatura del joystick"

    # _developer\developer.rpym:38
    old "Developer Menu"
    new "Developer Menu"

    # _developer\developer.rpym:43
    old "Interactive Director (D)"
    new "Interactive Director (D)"

    # _developer\developer.rpym:45
    old "Reload Game (Shift+R)"
    new "Reload Game (Shift+R)"

    # _developer\developer.rpym:47
    old "Console (Shift+O)"
    new "Console (Shift+O)"

    # _developer\developer.rpym:49
    old "Variable Viewer"
    new "Variable Viewer"

    # _developer\developer.rpym:51
    old "Image Location Picker"
    new "Image Location Picker"

    # _developer\developer.rpym:53
    old "Filename List"
    new "Filename List"

    # _developer\developer.rpym:57
    old "Show Image Load Log (F4)"
    new "Show Image Load Log (F4)"

    # _developer\developer.rpym:60
    old "Hide Image Load Log (F4)"
    new "Hide Image Load Log (F4)"

    # _developer\developer.rpym:66
    old "Return"
    new "Invio"

    # _developer\developer.rpym:95
    old "Nothing to inspect."
    new "Nothing to inspect."

    # _developer\developer.rpym:223
    old "Return to the developer menu"
    new "Return to the developer menu"

    # _developer\developer.rpym:383
    old "Rectangle: %r"
    new "Rectangle: %r"

    # _developer\developer.rpym:388
    old "Mouse position: %r"
    new "Mouse position: %r"

    # _developer\developer.rpym:393
    old "Right-click or escape to quit."
    new "Right-click or escape to quit."

    # _developer\developer.rpym:425
    old "Rectangle copied to clipboard."
    new "Rectangle copied to clipboard."

    # _developer\developer.rpym:428
    old "Position copied to clipboard."
    new "Position copied to clipboard."

    # _developer\developer.rpym:447
    old "Type to filter: "
    new "Type to filter: "

    # _developer\developer.rpym:572
    old "Textures: [tex_count] ([tex_size_mb:.1f] MB)"
    new "Textures: [tex_count] ([tex_size_mb:.1f] MB)"

    # _developer\developer.rpym:576
    old "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"
    new "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"

    # _developer\developer.rpym:586
    old "✔ "
    new "✔ "

    # _developer\developer.rpym:589
    old "✘ "
    new "✘ "

    # _developer\developer.rpym:594
    old "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"
    new "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"

    # _developer\inspector.rpym:38
    old "Displayable Inspector"
    new "Displayable Inspector"

    # _developer\inspector.rpym:61
    old "Size"
    new "Size"

    # _developer\inspector.rpym:65
    old "Style"
    new "Style"

    # _developer\inspector.rpym:71
    old "Location"
    new "Location"

    # _developer\inspector.rpym:122
    old "Inspecting Styles of [displayable_name!q]"
    new "Inspecting Styles of [displayable_name!q]"

    # _developer\inspector.rpym:139
    old "displayable:"
    new "displayable:"

    # _developer\inspector.rpym:145
    old "        (no properties affect the displayable)"
    new "        (no properties affect the displayable)"

    # _developer\inspector.rpym:147
    old "        (default properties omitted)"
    new "        (default properties omitted)"

    # _developer\inspector.rpym:185
    old "<repr() failed>"
    new "<repr() failed>"

    # _layout\classic_load_save.rpym:170
    old "a"
    new "a"

    # _layout\classic_load_save.rpym:179
    old "q"
    new "q"

    # 00iap.rpy:217
    old "Contacting App Store\nPlease Wait..."
    new "Connessione all'App Store\nAspetta per favore..."

    # 00updater.rpy:372
    old "The Ren'Py Updater is not supported on mobile devices."
    new "The Ren'Py Updater is not supported on mobile devices."

    # 00updater.rpy:491
    old "An error is being simulated."
    new "An error is being simulated."

    # 00updater.rpy:667
    old "Either this project does not support updating, or the update status file was deleted."
    new "O questo progetto non supporta l'aggiornamento o il file di stato dell'aggiornamento è stato cancellato."

    # 00updater.rpy:681
    old "This account does not have permission to perform an update."
    new "Questo account non ha l'autorizzazione per eseguire un aggiornamento."

    # 00updater.rpy:684
    old "This account does not have permission to write the update log."
    new "Questo account non dispone dell'autorizzazione per scrivere il registro di aggiornamento."

    # 00updater.rpy:711
    old "Could not verify update signature."
    new "Impossibile verificare la firma di aggiornamento."

    # 00updater.rpy:986
    old "The update file was not downloaded."
    new "Il file di aggiornamento non è stato scaricato."

    # 00updater.rpy:1004
    old "The update file does not have the correct digest - it may have been corrupted."
    new "Il file di aggiornamento non ha il digest corretto: potrebbe essere stato danneggiato."

    # 00updater.rpy:1060
    old "While unpacking {}, unknown type {}."
    new "Durante l'estrazione {}, tipo sconosciuto {}."

    # 00updater.rpy:1407
    old "Updater"
    new "Aggiornare"

    # 00updater.rpy:1414
    old "An error has occured:"
    new "C'è stato un errore:"

    # 00updater.rpy:1416
    old "Checking for updates."
    new "Verifica aggiornamenti."

    # 00updater.rpy:1418
    old "This program is up to date."
    new "Questo programma è aggiornato."

    # 00updater.rpy:1420
    old "[u.version] is available. Do you want to install it?"
    new "[u.version] è disponibile. Vuoi installarlo?"

    # 00updater.rpy:1422
    old "Preparing to download the updates."
    new "Preparazione per scaricare gli aggiornamenti."

    # 00updater.rpy:1424
    old "Downloading the updates."
    new "Download degli aggiornamenti."

    # 00updater.rpy:1426
    old "Unpacking the updates."
    new "Estrazione degli aggiornamenti."

    # 00updater.rpy:1428
    old "Finishing up."
    new "Finendo."

    # 00updater.rpy:1430
    old "The updates have been installed. The program will restart."
    new "Gli aggiornamenti sono stati installati. Il programma si riavvierà."

    # 00updater.rpy:1432
    old "The updates have been installed."
    new "Gli aggiornamenti sono stati installati."

    # 00updater.rpy:1434
    old "The updates were cancelled."
    new "Gli aggiornamenti sono stati annullati."

    # 00updater.rpy:1449
    old "Proceed"
    new "Procedere"

    # 00gallery.rpy:573
    old "Image [index] of [count] locked."
    new "Immagine [index] di [count] bloccato."

    # 00gallery.rpy:593
    old "prev"
    new "prec"

    # 00gallery.rpy:594
    old "next"
    new "pros"

    # 00gallery.rpy:595
    old "slideshow"
    new "Presentazione"

    # 00gallery.rpy:596
    old "return"
    new "invio"

    # 00gltest.rpy:70
    old "Renderer"
    new "Renderer"

    # 00gltest.rpy:74
    old "Automatically Choose"
    new "Scegli automaticamente"

    # 00gltest.rpy:79
    old "Force Angle/DirectX Renderer"
    new "Forza Angle/DirectX rendering"

    # 00gltest.rpy:83
    old "Force OpenGL Renderer"
    new "Forza OpenGL Rendering"

    # 00gltest.rpy:87
    old "Force Software Renderer"
    new "Forza Programma di Rendering"

    # 00gltest.rpy:93
    old "NPOT"
    new "NPOT"

    # 00gltest.rpy:97
    old "Enable"
    new "Abilitare"

    # 00gltest.rpy:131
    old "Powersave"
    new "Powersave"

    # 00gltest.rpy:145
    old "Framerate"
    new "Framerate"

    # 00gltest.rpy:149
    old "Screen"
    new "Screen"

    # 00gltest.rpy:153
    old "60"
    new "60"

    # 00gltest.rpy:157
    old "30"
    new "30"

    # 00gltest.rpy:163
    old "Tearing"
    new "Tearing"

    # 00gltest.rpy:179
    old "Changes will take effect the next time this program is run."
    new "Le modifiche diventeranno effettive al prossimo avvio di questo programma."

    # 00gltest.rpy:213
    old "Performance Warning"
    new "Avvertimento sulle prestazioni"

    # 00gltest.rpy:218
    old "This computer is using software rendering."
    new "Questo computer sta usando il programma di rendering."

    # 00gltest.rpy:220
    old "This computer is not using shaders."
    new "Questo computer non usa gli shader."

    # 00gltest.rpy:222
    old "This computer is displaying graphics slowly."
    new "Questo computer sta visualizzando la grafica lentamente."

    # 00gltest.rpy:224
    old "This computer has a problem displaying graphics: [problem]."
    new "Questo computer ha un problema nella visualizzazione della grafica: [problem]."

    # 00gltest.rpy:229
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display. Updating DirectX could fix this problem."
    new "I driver grafici potrebbero non essere aggiornati o non funzionare correttamente. Ciò può portare a una visualizzazione grafica lenta o errata. L'aggiornamento di DirectX potrebbe risolvere questo problema."

    # 00gltest.rpy:231
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display."
    new "I driver grafici potrebbero non essere aggiornati o non funzionare correttamente. Ciò può portare a una visualizzazione grafica lenta o errata."

    # 00gltest.rpy:236
    old "Update DirectX"
    new "Aggiornare DirectX"

    # 00gltest.rpy:242
    old "Continue, Show this warning again"
    new "Continua, mostra di nuovo questo avvertimento"

    # 00gltest.rpy:246
    old "Continue, Don't show warning again"
    new "Continua, non mostrare più l'avvertimento"

    # 00gltest.rpy:264
    old "Updating DirectX."
    new "Aggiornamento di DirectX."

    # 00gltest.rpy:268
    old "DirectX web setup has been started. It may start minimized in the taskbar. Please follow the prompts to install DirectX."
    new "È stata avviata la configurazione Web di DirectX. Potrebbe essere ridotto a icona nella barra delle applicazioni. Si prega di seguire le istruzioni per installare DirectX."

    # 00gltest.rpy:272
    old "{b}Note:{/b} Microsoft's DirectX web setup program will, by default, install the Bing toolbar. If you do not want this toolbar, uncheck the appropriate box."
    new "{b}Nota:{/b} il programma di installazione Web di Microsoft DirectX installerà, per impostazione predefinita, la barra degli strumenti di Bing. Se non vuoi questa barra degli strumenti, deseleziona la casella appropriata."

    # 00gltest.rpy:276
    old "When setup finishes, please click below to restart this program."
    new "Al termine dell'installazione, fai clic di sotto per riavviare questo programma."

    # 00gltest.rpy:278
    old "Restart"
    new "Ricomincia"

    # 00gamepad.rpy:32
    old "Select Gamepad to Calibrate"
    new "Seleziona il gamepad per calibrare"

    # 00gamepad.rpy:35
    old "No Gamepads Available"
    new "Nessun gamepad disponibile"

    # 00gamepad.rpy:54
    old "Calibrating [name] ([i]/[total])"
    new "Calibrazione [name] ([i]/[total])"

    # 00gamepad.rpy:58
    old "Press or move the [control!r] [kind]."
    new "Premere o spostare il [control!r] [kind]."

    # 00gamepad.rpy:66
    old "Skip (A)"
    new "Salta (A)"

    # 00gamepad.rpy:69
    old "Back (B)"
    new "Indietro (B)"

    # _errorhandling.rpym:528
    old "Open"
    new "Aprire"

    # _errorhandling.rpym:530
    old "Opens the traceback.txt file in a text editor."
    new "Aprire il file traceback.txt con un programma per editare i file di testo."

    # _errorhandling.rpym:532
    old "Copy"
    new "Copy"

    # _errorhandling.rpym:534
    old "Copies the traceback.txt file to the clipboard."
    new "Copies the traceback.txt file to the clipboard."

    # _errorhandling.rpym:561
    old "An exception has occurred."
    new "È avvenuta un'eccezione."

    # _errorhandling.rpym:581
    old "Rollback"
    new "Tornare indietro"

    # _errorhandling.rpym:583
    old "Attempts a roll back to a prior time, allowing you to save or choose a different choice."
    new "Tentativo di tornare indietro ad un tempo precedente, permettendoti di salvare o scegliere scelte differenti."

    # _errorhandling.rpym:586
    old "Ignore"
    new "Ignorare"

    # _errorhandling.rpym:590
    old "Ignores the exception, allowing you to continue."
    new "Ignorare le eccezioni, permettendoti di continuare."

    # _errorhandling.rpym:592
    old "Ignores the exception, allowing you to continue. This often leads to additional errors."
    new "Ignorare le eccezioni, permettendoti di continuare. Questo spesso porta ad errori aggiuntivi."

    # _errorhandling.rpym:596
    old "Reload"
    new "Ricaricare"

    # _errorhandling.rpym:598
    old "Reloads the game from disk, saving and restoring game state if possible."
    new "Ricaricare il gioca dal disco, salvare e ripristinare lo stato del gioco se possibile."

    # _errorhandling.rpym:601
    old "Console"
    new "Console"

    # _errorhandling.rpym:603
    old "Opens a console to allow debugging the problem."
    new "Aprire la console per permetterti di correggere il problema."

    # _errorhandling.rpym:613
    old "Quits the game."
    new "Uscire dal gioco."

    # _errorhandling.rpym:637
    old "Parsing the script failed."
    new "L'analisi dello script ha avuto esito negativo."

    # _errorhandling.rpym:663
    old "Opens the errors.txt file in a text editor."
    new "Aprire il file errors.txt con un programma per editare il file di testo."

    # _errorhandling.rpym:667
    old "Copies the errors.txt file to the clipboard."
    new "Copies the errors.txt file to the clipboard."

# TODO: Translation updated at 2019-04-23 10:50

translate italian strings:

    # 00accessibility.rpy:76
    old "Font Override"
    new "Font Override"

    # 00accessibility.rpy:80
    old "Default"
    new "Default"

    # 00accessibility.rpy:84
    old "DejaVu Sans"
    new "DejaVu Sans"

    # 00accessibility.rpy:88
    old "Opendyslexic"
    new "Opendyslexic"

    # 00accessibility.rpy:94
    old "Text Size Scaling"
    new "Text Size Scaling"

    # 00accessibility.rpy:100
    old "Reset"
    new "Reset"

    # 00accessibility.rpy:105
    old "Line Spacing Scaling"
    new "Line Spacing Scaling"

    # 00accessibility.rpy:117
    old "Self-Voicing"
    new "Self-Voicing"

    # 00accessibility.rpy:121
    old "Off"
    new "Off"

    # 00accessibility.rpy:125
    old "Text-to-speech"
    new "Text-to-speech"

    # 00accessibility.rpy:129
    old "Clipboard"
    new "Clipboard"

    # 00accessibility.rpy:133
    old "Debug"
    new "Debug"

    # 00action_file.rpy:353
    old "Save slot %s: [text]"
    new "Save slot %s: [text]"

    # 00action_file.rpy:434
    old "Load slot %s: [text]"
    new "Load slot %s: [text]"

    # 00action_file.rpy:487
    old "Delete slot [text]"
    new "Delete slot [text]"

    # 00action_file.rpy:569
    old "File page auto"
    new "File page auto"

    # 00action_file.rpy:571
    old "File page quick"
    new "File page quick"

    # 00action_file.rpy:573
    old "File page [text]"
    new "File page [text]"

    # 00action_file.rpy:772
    old "Next file page."
    new "Next file page."

    # 00action_file.rpy:845
    old "Previous file page."
    new "Previous file page."

    # 00action_file.rpy:924
    old "Quick save."
    new "Quick save."

    # 00action_file.rpy:943
    old "Quick load."
    new "Quick load."

    # 00action_other.rpy:355
    old "Language [text]"
    new "Language [text]"

    # 00director.rpy:1481
    old "⬆"
    new "⬆"

    # 00director.rpy:1487
    old "⬇"
    new "⬇"

    # 00library.rpy:179
    old "bar"
    new "bar"

    # 00library.rpy:180
    old "selected"
    new "selected"

    # 00library.rpy:181
    old "viewport"
    new "viewport"

    # 00library.rpy:182
    old "horizontal scroll"
    new "horizontal scroll"

    # 00library.rpy:183
    old "vertical scroll"
    new "vertical scroll"

    # 00library.rpy:184
    old "activate"
    new "activate"

    # 00library.rpy:185
    old "deactivate"
    new "deactivate"

    # 00library.rpy:186
    old "increase"
    new "increase"

    # 00library.rpy:187
    old "decrease"
    new "decrease"

    # 00preferences.rpy:233
    old "display"
    new "display"

    # 00preferences.rpy:245
    old "transitions"
    new "transitions"

    # 00preferences.rpy:254
    old "skip transitions"
    new "skip transitions"

    # 00preferences.rpy:256
    old "video sprites"
    new "video sprites"

    # 00preferences.rpy:265
    old "show empty window"
    new "show empty window"

    # 00preferences.rpy:274
    old "text speed"
    new "text speed"

    # 00preferences.rpy:282
    old "joystick"
    new "joystick"

    # 00preferences.rpy:282
    old "joystick..."
    new "joystick..."

    # 00preferences.rpy:289
    old "skip"
    new "skip"

    # 00preferences.rpy:292
    old "skip unseen [text]"
    new "skip unseen [text]"

    # 00preferences.rpy:297
    old "skip unseen text"
    new "skip unseen text"

    # 00preferences.rpy:299
    old "begin skipping"
    new "begin skipping"

    # 00preferences.rpy:303
    old "after choices"
    new "after choices"

    # 00preferences.rpy:310
    old "skip after choices"
    new "skip after choices"

    # 00preferences.rpy:312
    old "auto-forward time"
    new "auto-forward time"

    # 00preferences.rpy:326
    old "auto-forward"
    new "auto-forward"

    # 00preferences.rpy:336
    old "auto-forward after click"
    new "auto-forward after click"

    # 00preferences.rpy:345
    old "automatic move"
    new "automatic move"

    # 00preferences.rpy:354
    old "wait for voice"
    new "wait for voice"

    # 00preferences.rpy:363
    old "voice sustain"
    new "voice sustain"

    # 00preferences.rpy:372
    old "self voicing"
    new "self voicing"

    # 00preferences.rpy:381
    old "clipboard voicing"
    new "clipboard voicing"

    # 00preferences.rpy:390
    old "debug voicing"
    new "debug voicing"

    # 00preferences.rpy:399
    old "emphasize audio"
    new "emphasize audio"

    # 00preferences.rpy:408
    old "rollback side"
    new "rollback side"

    # 00preferences.rpy:418
    old "gl powersave"
    new "gl powersave"

    # 00preferences.rpy:424
    old "gl framerate"
    new "gl framerate"

    # 00preferences.rpy:427
    old "gl tearing"
    new "gl tearing"

    # 00preferences.rpy:430
    old "font transform"
    new "font transform"

    # 00preferences.rpy:433
    old "font size"
    new "font size"

    # 00preferences.rpy:441
    old "font line spacing"
    new "font line spacing"

    # 00preferences.rpy:460
    old "music volume"
    new "music volume"

    # 00preferences.rpy:461
    old "sound volume"
    new "sound volume"

    # 00preferences.rpy:462
    old "voice volume"
    new "voice volume"

    # 00preferences.rpy:463
    old "mute music"
    new "mute music"

    # 00preferences.rpy:464
    old "mute sound"
    new "mute sound"

    # 00preferences.rpy:465
    old "mute voice"
    new "mute voice"

    # 00preferences.rpy:466
    old "mute all"
    new "mute all"

    # _developer\developer.rpym:63
    old "Image Attributes"
    new "Image Attributes"

    # _developer\developer.rpym:90
    old "[name] [attributes] (hidden)"
    new "[name] [attributes] (hidden)"

    # _developer\developer.rpym:94
    old "[name] [attributes]"
    new "[name] [attributes]"

    # _developer\developer.rpym:154
    old "Hide deleted"
    new "Hide deleted"

    # _developer\developer.rpym:154
    old "Show deleted"
    new "Show deleted"

    # _errorhandling.rpym:542
    old "Copy BBCode"
    new "Copy BBCode"

    # _errorhandling.rpym:544
    old "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:546
    old "Copy Markdown"
    new "Copy Markdown"

    # _errorhandling.rpym:548
    old "Copies the traceback.txt file to the clipboard as Markdown for Discord."
    new "Copies the traceback.txt file to the clipboard as Markdown for Discord."

    # _errorhandling.rpym:683
    old "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:687
    old "Copies the errors.txt file to the clipboard as Markdown for Discord."
    new "Copies the errors.txt file to the clipboard as Markdown for Discord."

# TODO: Translation updated at 2019-07-20 12:06

translate italian strings:

    # renpy/common/00accessibility.rpy:191
    old "The options on this menu are intended to improve accessibility. They may not work with all games, and some combinations of options may render the game unplayable. This is not an issue with the game or engine. For the best results when changing fonts, try to keep the text size the same as it originally was."
    new "The options on this menu are intended to improve accessibility. They may not work with all games, and some combinations of options may render the game unplayable. This is not an issue with the game or engine. For the best results when changing fonts, try to keep the text size the same as it originally was."

