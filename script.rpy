﻿# TODO: Translation updated at 2018-03-28 12:56

# game/script.rpy:260
translate italian konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "Cosa diavolo sto facendo qui?..."

# game/script.rpy:296
translate italian gjconnect_4ae60ea0:

    # "Disconnected."
    "Disconnected."

# game/script.rpy:325
translate italian gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Connessione a Game Jolt riuscita.\nAVVERTENZA: la connessione non conta per i giochi salvati in precedenza."

translate italian strings:

    # script.rpy:209
    old "Master"
    new "Maestro"

    # script.rpy:210
    old "Big brother"
    new "Grande Fratello"

    # script.rpy:281
    old "Disconnect from Game Jolt?"
    new "Disconnettersi da Game Jolt?"

    # script.rpy:281
    old "Yes, disconnect."
    new "Sì, disconnetti."

    # script.rpy:281
    old "No, return to menu."
    new "No, torna al menu."

    # script.rpy:343
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "Si è verificato un problema Forse la tua connessione ha un problema. O forse è Game Jolt che deroga...\n\nProva ancora?"

    # script.rpy:343
    old "Yes, try again."
    new "Sì, riprova."

    # script.rpy:361
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "Sembra che l'autenticazione sia fallita. Forse non hai scritto correttamente lo username e il token...\n\nProva ancora?"

    # script.rpy:228
    old "Please enter your name and press Enter:"
    new "Inserisci il tuo nome e premi Invio:"

    # script.rpy:295
    old "Type here your username and press Enter."
    new "Scrivi qui il tuo nome utente e premi Invio."

    # script.rpy:296
    old "Now, type here your game token and press Enter."
    new "Ora digita qui il token di gioco e premi Invio."

    # script.rpy:207
    old "Game Jolt trophy obtained!"
    new "Il trofeo di Game Jolt è stato ottenuto!"

    # script.rpy:180
    old "Sakura's mom"
    new "Mamma di S."

    # script.rpy:181
    old "Sakura's dad"
    new "Papà di S."

    # script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}Grazie per aver giocato!"
# TODO: Translation updated at 2019-04-23 10:50

# game/script.rpy:286
translate italian update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:300
translate italian gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

translate italian strings:

    # script.rpy:192
    old "Sakura"
    new "Sakura"

    # script.rpy:193
    old "Rika"
    new "Rika"

    # script.rpy:194
    old "Nanami"
    new "Nanami"

    # script.rpy:197
    old "Toshio"
    new "Toshio"

    # script.rpy:202
    old "Taichi"
    new "Taichi"

# TODO: Translation updated at 2019-04-27 11:13

translate italian strings:

    # script.rpy:226
    old "Achievement obtained!"
    new "Achievement obtained!"

